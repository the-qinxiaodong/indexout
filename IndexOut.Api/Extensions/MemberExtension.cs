﻿using IndexOut.Api.Entities;
using System.Reflection;

namespace IndexOut.Api.Extensions
{
    public static class MemberExtension
    {
        private static Type _memberType;
        private static PropertyInfo[] _propertiesInfos;

        private static Func<dynamic?, bool> _defaultPredicate = aValue => aValue != null;

        static MemberExtension()
        {
            _memberType = typeof(Member);
            _propertiesInfos = _memberType.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.SetProperty);
        }

        public static void CopyTo(this Member source, Member distination, Func<dynamic?, bool>? checkValuePredicate = null, params string[] namesOfWillIgnoreField)
        {
            var predicate = checkValuePredicate ?? _defaultPredicate;

            foreach (var propertyInfo in _propertiesInfos)
            {
                var sourceValue = propertyInfo.GetValue(source);
                if (predicate(sourceValue) && !namesOfWillIgnoreField.Contains(propertyInfo.Name))
                    propertyInfo.SetValue(distination, sourceValue);
            }
        }

    }
}
