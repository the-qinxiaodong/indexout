﻿using Microsoft.AspNetCore.Mvc;

namespace IndexOut.Api.Extensions
{
    public static class ControllerBaseExtensions
    {
        public static int? GetCurrentUserId(this ControllerBase controllerBase)
        {
            var idString = controllerBase.HttpContext.Items["memberId"];
            var success = int.TryParse(idString as string, out int id);

            return success ? id : null;
        }
    }
}
