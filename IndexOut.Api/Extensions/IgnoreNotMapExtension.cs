﻿using AutoMapper;
using IndexOut.Api.Attributes;

namespace IndexOut.Api.Extensions
{
    public static class IgnoreNotMapExtension
    {
        public static IMappingExpression<TSource, TDestination> IgnoreNoMap<TSource, TDestination>
            (this IMappingExpression<TSource, TDestination> expression)
        {
            var sourceTypeInfos = typeof(TSource);
            var sourcePropertyInfos = sourceTypeInfos.GetProperties();
            var newExpression = expression;
            foreach (var propertyInfo in sourcePropertyInfos)
            {
                var noMapAttributes = propertyInfo.GetCustomAttributes(typeof(NoMapAttribute), true);

                if (noMapAttributes != null &&
                    noMapAttributes.Length > 0)
                    newExpression = expression.ForMember(propertyInfo.Name, opt => opt.Ignore());
            }

            return newExpression!;
        }
    }
}
