﻿using IndexOut.Api.Filters;
using Microsoft.AspNetCore.Mvc;
using IndexOut.Api.Extensions;
using IndexOut.Api.Repositories;

namespace IndexOut.Api.Controllers
{
    [Route("/api/vote")]
    [ApiController]
    [ServiceFilter(typeof(LoginRequiredAsyncFilterAttribute))]
    public class VoteController : ControllerBase
    {
        private VoteOptionRepository _voteOptionRepository;

        public VoteController(VoteOptionRepository voteOptionRepository)
        {
            _voteOptionRepository = voteOptionRepository;
        }

        [HttpPost("{voteId:int}/on")]
        public async Task<ActionResult> Vote(int voteId, int optionId)
        {
            var id = this.GetCurrentUserId();

            if (id == null)
                return Unauthorized();

            var option = await _voteOptionRepository.FindByIDAsync(optionId);
            if (option == null || option.Vote!.Id != voteId)
                return NotFound();

            option.VoteNumber += 1;
            return Ok();
        }
    }
}
