﻿using IndexOut.Api.Entities;
using IndexOut.Api.Models;
using Microsoft.AspNetCore.Mvc;

namespace IndexOut.Api.Controllers
{
    public partial class MemberController : ControllerBase
    {

        [HttpPost("vote")]
        public async Task<IActionResult> CreateVote(int memberId, CreationVoteModel creationVoteModel)
        {
            var vote = _mapper.Map<Vote>(creationVoteModel);
            var topic = await _topicRepository.FindByIDAsync(creationVoteModel.Topic);
            var author = await _memberRepository.FindByIDAsync(memberId);

            vote.Author = author;
            vote.Topic = topic;
            foreach (var option in vote.Options)
                option.Vote = vote;

            await _voteRepository.SaveAsync(vote);
            return Ok(_mapper.Map<VoteModel>(vote));
        }

        [HttpGet("vote/{voteId:int}")]
        public async Task<IActionResult> GetVote(int memberId, int voteId)
        {
            var vote = await _voteRepository.FindByIDAsync(voteId);
            if (vote == null || vote.Author!.Id != memberId)
                return NotFound();

            return Ok(_mapper.Map<VoteModel>(vote));
        }

        [HttpPost("vote/{voteId:int}/release")]
        public async Task<IActionResult> ReleaseVote(int memberId, int voteId, DateTime endTime)
        {
            var vote = await _voteRepository.FindByIDAsync(voteId);
            if (vote == null || vote.Author!.Id != memberId)
                return NotFound();

            if (endTime < DateTime.Now)
                return BadRequest();

            vote.PublishTime = DateTime.Now;
            vote.State = VoteState.Published;
            vote.EndTime = endTime;

            return Ok(_mapper.Map<VoteModel>(vote));
        }
    }
}
