﻿using AutoMapper;
using IndexOut.Api.Models;
using IndexOut.Api.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace IndexOut.Api.Controllers
{
    [Route("/problem")]
    [ApiController]
    public class ProblemController : ControllerBase
    {
        private readonly ProblemRepository _problemRepository;
        private readonly IMapper _mapper;

        public ProblemController(IMapper mapper, ProblemRepository problemRepository)
        {
            _mapper = mapper;
            _problemRepository = problemRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetProblemPagination(int pageNumber = 1, int pageSize = 10)
        {
            if (pageNumber < 1 || pageSize < 1)
                return BadRequest();

            var problems = await _problemRepository.GetPaginationAsync(pageNumber, pageSize);
            return Ok(_mapper.Map<IEnumerable<ProblemModel>>(problems));
        }
    }
}
