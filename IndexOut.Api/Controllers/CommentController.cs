﻿using AutoMapper;
using IndexOut.Api.Models;
using IndexOut.Api.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace IndexOut.Api.Controllers
{
    [Route("api/comment")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly CommentRepository _commentRepository;
        private readonly IMapper _mapper;

        public CommentController(IMapper mapper, CommentRepository commentRepository)
        {
            _mapper = mapper;
            _commentRepository = commentRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetCommentPagination(int commentedId, int pageNumber = 1, int pageSize = 10)
        {
            var comments = await _commentRepository.GetPaginationAsync(pageNumber, pageSize, comment => comment.To!.Id == commentedId);
            return Ok(_mapper.Map<IEnumerable<CommentModel>>(comments));
        }
    }
}
