﻿using AutoMapper;
using IndexOut.Api.Filters;
using IndexOut.Api.Models;
using IndexOut.Api.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IndexOut.Api.Controllers
{
    [Route("api/member/{memberId:int}")]
    [ApiController]
    [ServiceFilter(typeof(LoginRequiredAsyncFilterAttribute))]
    public partial class MemberController : ControllerBase
    {
        private readonly MemberRespository _memberRepository;
        private readonly ArticleRepository _articleRepository;
        private readonly DbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ProblemRepository _problemRepository;
        private readonly TagRepository _tagRepository;
        private readonly TopicRepository _topicRepository;
        private readonly VoteRepository _voteRepository;

        public MemberController(MemberRespository memberRepository, ArticleRepository articleRepository, DbContext dbContext, IMapper mapper, ProblemRepository problemRepository, TagRepository tagRepository, TopicRepository topicRepository, VoteRepository voteRepository)
        {
            _memberRepository = memberRepository;
            _articleRepository = articleRepository;
            _dbContext = dbContext;
            _mapper = mapper;
            _problemRepository = problemRepository;
            _tagRepository = tagRepository;
            _topicRepository = topicRepository;
            _voteRepository = voteRepository;
        }

        [HttpGet("profile")]
        public async Task<IActionResult> GetProfile(int memberId)
        {
            var (hasGot, profile) = await _memberRepository.TryGetProfileAsync(memberId);
            if (hasGot)
                return new JsonResult(profile);

            return BadRequest();
        }

        [HttpPut("password")]
        public async Task<IActionResult> ChangePassword(ChangePasswordModel changePasswordModle)
        {
            var uid = (Request.HttpContext.Items["memberId"] as int?)
                ?? throw new InvalidOperationException("当这种情况发生，意味着程序出现逻辑错误，请检查登陆过滤器！");
            var changed = await _memberRepository.TryChangePasswordAsync(changePasswordModle.NewPassword, changePasswordModle.OldPassword, uid);

            if (changed)
                return Ok();

            return BadRequest();
        }

        [HttpPut("profile")]
        public async Task<IActionResult> UpdateProfile(UpdateProfileModel updateProfileModel)
        {
            var updated = await _memberRepository.TryUpdateProfileAsync(updateProfileModel);

            if (updated)
                return Ok();

            return BadRequest();
        }

    }
}
