﻿using IndexOut.Api.Entities;
using IndexOut.Api.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace IndexOut.Api.Controllers
{
    //[Route("api/member/{memberId:int}")]
    //[ApiController]
    //[ServiceFilter(typeof(LoginRequiredAsyncFilterAttribute))]
    public partial class MemberController : ControllerBase
    {

        [HttpPost("article")]
        public async Task<IActionResult> CreateArticleAsync(int memberId, CreationArticleModel creationArticleModel)
        {
            var (created, article) = await _articleRepository.TryCreateArticleAsync(creationArticleModel, memberId);

            return created ? Ok(article) : BadRequest();
        }

        [HttpPatch("article/{articleId:int}")]
        public async Task<IActionResult> UpdateArticleAsync(int articleId, int memberId, JsonPatchDocument<Article> patchDocument)
        {
            var article = await _articleRepository.FindByAuthorAndIdAsync(memberId, articleId);
            if (article == null)
                return NotFound();

            var trackingArticle = _dbContext.Attach(article);
            //TODO 处理PatchDocument应用错误
            // TODO 处理文章标签的添加和删除
            patchDocument.ApplyTo(article);
            await _articleRepository.UpdateAsync(trackingArticle);
            return Ok(_mapper.Map<ArticleModel>(article));
        }

        [HttpGet("article")]
        public async Task<IActionResult> GetArticlesPaginationAsync(int memberId, int pageNumber = 1, int pageSize = 10)
        {
            var isValidPaginationArguments = pageNumber >= 1 && pageSize > 0;
            if (!isValidPaginationArguments)
                return BadRequest();

            var articles = await _articleRepository.GetPaginationOfAuthorAsync(memberId, pageNumber, pageSize);
            var articleDtos = _mapper.Map<ArticleModel[]>(articles);
            return Ok(articleDtos);
        }

        [HttpDelete("article/{articleId:int}")]
        public async Task<IActionResult> DeleteArticleAsync(int memberId, int articleId)
        {
            var article = await _articleRepository.FindByIDAsync(articleId);

            if (article != null && article.Id == memberId)
            {
                await _articleRepository.DeleteAsync(article);
                return Ok();
            }

            return NotFound();
        }

        [HttpGet("article/{articleId:int}")]
        public async Task<IActionResult> GetArticleAsync(int memberId, int articleId)
        {
            var article = await _articleRepository.FindByIDAndLoadRelationshipAsync(articleId);
            if (article == null)
                return NotFound();

            if (article.Author!.Id != memberId)
                return BadRequest();

            return Ok(_mapper.Map<ArticleModel>(article));
        }

    }
}
