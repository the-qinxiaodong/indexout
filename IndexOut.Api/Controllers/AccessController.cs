﻿using AutoMapper;
using IndexOut.Api.Models;
using IndexOut.Api.Repositories;
using IndexOut.Api.Services;
using IndexOut.Api.Utils;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Net.Mail;

namespace IndexOut.Api.Controllers
{
    [Route("api/access")]
    [ApiController]
    public class AccessController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly MemberRespository _memberRepository;
        private readonly JwtTokenUtil _jwtUtil;
        private readonly RegistrationCodeRepository _registrationCodeRepository;
        private readonly EmailService _emailService;

        public AccessController(IMapper mapper, MemberRespository repository, JwtTokenUtil jwtUtil, RegistrationCodeRepository registrationCodeRepository, EmailService emailService)
        {
            _mapper = mapper;
            _memberRepository = repository;
            _jwtUtil = jwtUtil;
            _registrationCodeRepository = registrationCodeRepository;
            _emailService = emailService;
        }

        [HttpPost("registration-code")]
        public async Task<IActionResult> ApplyForRegistrationCodeAsync([FromQuery][DataType(DataType.EmailAddress)] string mailAddress)
        {
            var (isValidEmail, registrationCode) = await _registrationCodeRepository.TryApplyForRegistrationCodeAsync(new MailAddress(mailAddress));

            if (isValidEmail)
            {
                await _emailService.SendRegistrationCodeEmailAsync(registrationCode!.Value, new MailAddress(mailAddress));
                return Ok();
            }
            else
            {
                return new JsonResult(new
                {
                    Errors = "邮箱已被注册!"
                })
                {
                    StatusCode = StatusCodes.Status403Forbidden,
                };
            }
        }

        [HttpPost("register")]
        public async Task<IActionResult> RegisterAsync(RegisterModel registerModel)
        {
            var hasRegistered = await _memberRepository.RegisterAsync(registerModel);

            return hasRegistered ? Ok() : BadRequest();
        }

        [HttpPost("login")]
        public async Task<IActionResult> LoginAsync(LoginModel loginModel)
        {
            var (hasLogined, member) = await _memberRepository.LoginAsync(loginModel);

            if (hasLogined)
            {
                return new JsonResult(new
                {
                    Id = member!.Id,
                    Token = _jwtUtil.Make(member!.Id.ToString())
                });
            }
            else
            {
                return new JsonResult(new
                {
                    Error = "用户名/邮箱或密码错误"
                })
                {
                    StatusCode = StatusCodes.Status401Unauthorized,
                };
            }
        }

        [HttpPost("reset-password-token")]
        public async Task<IActionResult> ApplyForResetPassword([FromQuery] ApplyForResetPasswordModel resetPasswordModel)
        {
            var sended = await _memberRepository.TrySendResetPasswordEmailIfValidModelAsync(resetPasswordModel);

            return sended ? Ok() : NotFound();
        }

        [HttpPost("reset-password")]
        public async Task<IActionResult> ResetPassword(ResetPasswordModel resetPasswordModel)
        {
            var hasReseted = await _memberRepository.TryResetPassword(resetPasswordModel);

            return hasReseted ? Ok() : BadRequest();
        }
    }
}
