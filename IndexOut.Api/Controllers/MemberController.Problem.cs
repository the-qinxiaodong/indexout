﻿using IndexOut.Api.Entities;
using IndexOut.Api.Models;
using Microsoft.AspNetCore.Mvc;

namespace IndexOut.Api.Controllers
{
    public partial class MemberController : ControllerBase
    {

        [HttpGet("problem/{problemId:int}")]
        public async Task<IActionResult> GetProblem(int memberId, int problemId)
        {
            var problem = await _problemRepository.GetProblemAsync(problemId);
            if (problem == null || problem.Author?.Id != memberId)
                return NotFound();

            return Ok(_mapper.Map<ProblemModel>(problem));
        }

        [HttpPost("problem")]
        public async Task<IActionResult> CreateProblem(int memberId, CreationProblemModel creationProblemModel)
        {
            var problem = _mapper.Map<Problem>(creationProblemModel) ?? throw new NotSupportedException($"不能完成映射: {nameof(creationProblemModel)}到{nameof(Problem)}");

            problem.Author = _dbContext.Attach(new Member() { Id = memberId }).Entity;
            var topic = await _topicRepository.FindTopicByValueAsync(problem.Topic!.Value);

            if (topic != null)
                problem.Topic = topic;

            await _problemRepository.SaveAsync(problem!);
            return Ok(_mapper.Map<ProblemModel>(problem));
        }

        [HttpGet("problem")]
        public async Task<IActionResult> GetProblemPagination(int memberId, int pageNumber = 1, int pageSize = 10)
        {
            var problems = await _problemRepository.GetPaginationAsync(pageNumber, pageSize, problem => problem.Author!.Id == memberId);
            return Ok(_mapper.Map<IEnumerable<ProblemModel>>(problems));
        }

        [HttpPost("problem/{problemId:int}/publish")]
        public async Task<IActionResult> PublishProblemAsync(int memberId, int problemId)
        {
            var problem = await _problemRepository.GetProblemAsync(problemId);
            if (problem == null || problem.Author?.Id != memberId)
                return NotFound();

            if (problem.State == ProblemState.Published)
                return BadRequest();

            problem.State = ProblemState.Published;
            problem.PublishTime = DateTime.Now;

            return Ok(_mapper.Map<ProblemModel>(problem));
        }

        [HttpPost("problem/{problemId:int}/edit")]
        public async Task<IActionResult> UpdateProblemAsync(int memberId, int problemId, UpdateProblemModel updateModel)
        {
            var problem = await _problemRepository.GetProblemAsync(problemId);
            if (problem == null || problem.Author!.Id != memberId)
                return NotFound();

            updateModel.ApplyTo(problem);
            return Ok(_mapper.Map<ProblemModel>(problem));
        }
    }
}
