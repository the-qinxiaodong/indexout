﻿namespace IndexOut.Api.Profiles
{
    public class EmailProfile
    {
        public const string EmailEnvVariableName = "Email:Address";
        public const string HostEnvVariableName = "Email:Host";
        public const string EmailPasswordEnvVariableName = "Email:Password";

        private readonly string _emailOfSender;
        private readonly string _host;
        private readonly string _password;

        public EmailProfile(IConfiguration configuration)
        {
            var email = configuration[EmailEnvVariableName];
            var host = configuration[HostEnvVariableName];
            var password = configuration[EmailPasswordEnvVariableName];

            _emailOfSender = email ?? throw new ArgumentException($"必须设置{EmailEnvVariableName}环境变量");
            _host = host ?? throw new ArgumentException($"必须设置{HostEnvVariableName}环境变量");
            _password = password ?? throw new ArgumentException($"必须设置{EmailPasswordEnvVariableName}环境变量");
        }

        public string Email { get => _emailOfSender; }
        public string Host { get => _host; }
        public string Password { get => _password; }
    }
}
