﻿using AutoMapper;
using IndexOut.Api.Entities;
using IndexOut.Api.Extensions;
using IndexOut.Api.Models;

namespace IndexOut.Api.Profiles
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<RegisterModel, Member>().ForMember(dest => dest.NickName, option => option.Condition(src => src.NickName != null));
            CreateMap<Member, MemberProfileModel>();
            CreateMap<UpdateProfileModel, Member>();
            CreateMap<CreationArticleModel, Article>().IgnoreNoMap();
            CreateMap<EntityBase, int>().ConvertUsing(entity => entity.Id);
            CreateMap<Tag, string>().ConvertUsing(tag => tag.Value);
            CreateMap<int, Tag>().ConvertUsing(id => new Tag() { Id = id });
            CreateMap<int, Topic>().ConvertUsing(id => new Topic() { Id = id });
            CreateMap<Article, ArticleModel>();
            CreateMap<CreationProblemModel, Problem>();
            CreateMap<Problem, ProblemModel>();
            CreateMap<Problem, UpdateProblemModel>();
            CreateMap<Member, MemberModel>();
            CreateMap<string, Tag>().ConvertUsing(stringValue => new Tag() { Value = stringValue, CreatedDate = DateTime.Now });
            CreateMap<string, Topic>().ConvertUsing(stringValue => new Topic() { Value = stringValue, CreatedDate = DateTime.Now });
            CreateMap<Topic, string>().ConvertUsing(topic => topic.Value);
            CreateMap<CreationVoteModel, Vote>();
            CreateMap<Vote, VoteModel>();
            CreateMap<string, VoteOption>().ConvertUsing(str => new VoteOption() { Value = str });
            CreateMap<VoteOption, string>().ConvertUsing(option => option.Value);
            CreateMap<PostCommentModel, Comment>();
            CreateMap<Comment, CommentModel>();
            CreateMap<Member, int>().ConstructUsing(member => member.Id);
        }
    }
}
