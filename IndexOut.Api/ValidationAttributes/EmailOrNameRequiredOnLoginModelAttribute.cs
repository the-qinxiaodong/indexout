﻿using IndexOut.Api.Models;
using System.ComponentModel.DataAnnotations;

namespace IndexOut.Api.ValidationAttributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class EmailOrNameRequiredOnLoginModelAttribute : ValidationAttribute
    {
        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            var loginModel = (validationContext.ObjectInstance as LoginModel) ?? throw new ArgumentException("只验证LoginModel");

            if (loginModel.Name == null && loginModel.Email == null)
            {
                return new ValidationResult(ErrorMessage, new[] { nameof(LoginModel) });
            }

            return ValidationResult.Success;
        }
    }
}
