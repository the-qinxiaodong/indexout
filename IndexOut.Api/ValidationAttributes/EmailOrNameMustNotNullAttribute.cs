﻿using System.ComponentModel.DataAnnotations;

namespace IndexOut.Api.ValidationAttributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class EmailOrNameMustNotNullAttribute : ValidationAttribute
    {
        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            var type = value!.GetType();
            var nameField = type.GetProperty("Name") ?? throw new InvalidOperationException("对象上没有Name属性");
            var emailField = type.GetProperty("Email") ?? throw new InvalidOperationException("对象上没有Email属性");
            var name = nameField!.GetValue(value);
            var email = emailField!.GetValue(value);

            if (name == email && email == null)
            {
                return new ValidationResult(ErrorMessage, new[] { nameof(type) });
            }

            return ValidationResult.Success;
        }
    }
}
