﻿using JWT;
using JWT.Algorithms;
using JWT.Exceptions;
using JWT.Serializers;
using System.Security.Cryptography;
using System.Text.Json;

namespace IndexOut.Api.Utils
{
    public class JwtBuilderWrapper
    {
        public const string SecretKeyConfigName = "JWT:SecretKey";

        private readonly string _secretKey;
        private static readonly JwtEncoder _jwtEncoder;
        private static readonly JwtDecoder _jwtDecoder;
        private static readonly RSA _rsaPublicKey;
        private static readonly RSA _rsaPrivateKey;

        static JwtBuilderWrapper()
        {
            _rsaPublicKey = RSACryptoServiceProvider.Create() ?? throw new NotSupportedException($"无法创建{nameof(RSACryptoServiceProvider)}实例");
            _rsaPrivateKey = RSACryptoServiceProvider.Create() ?? throw new NotSupportedException($"无法创建{nameof(RSACryptoServiceProvider)}实例");

            var algorithm = new RS256Algorithm(_rsaPublicKey, _rsaPrivateKey);
            var serializer = new JsonNetSerializer();
            var urlEncoder = new JwtBase64UrlEncoder();
            _jwtEncoder = new JwtEncoder(algorithm, serializer, urlEncoder);
            _jwtDecoder = new JwtDecoder(serializer, urlEncoder);
        }

        public JwtBuilderWrapper(IConfiguration configuration)
        {
            _secretKey = configuration[SecretKeyConfigName];
        }

        public string Encode<T>(T payload)
        {
            return _jwtEncoder.Encode(payload, _secretKey);
        }

        public TDestination Decode<TDestination>(string jwt)
        {
            var json = _jwtDecoder.Decode(jwt);
            var result = JsonSerializer.Deserialize<TDestination>(json);
            return result ?? throw new NotSupportedException($"反序列化为${nameof(TDestination)}失败");
        }

        public bool TryDecode<TDestination>(string jwt, out TDestination? result)
        {
            result = default;
            try
            {
                result = Decode<TDestination>(jwt);
                return true;
            }
            catch (ArgumentException _)
            {
                return false;
            }
            catch (NotSupportedException _)
            {
                return false;
            }
        }
    }
}
