﻿using IndexOut.Api.Profiles;
using System.Net;
using System.Net.Mail;

namespace IndexOut.Api.Utils
{
    public class SMTPClientWrapper
    {
        private readonly SmtpClient _smtpClient;
        private readonly EmailProfile _emailProfile;

        public SMTPClientWrapper(EmailProfile emailProfile)
        {
            _emailProfile = emailProfile;

            _smtpClient = new SmtpClient(_emailProfile.Host)
            {
                Credentials = new NetworkCredential(_emailProfile.Email, _emailProfile.Password),
                EnableSsl = true
            };
        }

        public async Task SendAsync(string title, string body, MailAddress to)
        {
            var message = new MailMessage()
            {
                From = new MailAddress(_emailProfile.Email),
                Subject = title,
                Body = body,
                IsBodyHtml = false
            };
            message.To.Add(to);
            await _smtpClient.SendMailAsync(message);
        }
    }
}
