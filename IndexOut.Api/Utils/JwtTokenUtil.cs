﻿using IndexOut.Api.Models;

namespace IndexOut.Api.Utils
{
    public class JwtTokenUtil
    {
        private readonly string _issuer;
        private readonly string _audience;
        private readonly JwtBuilderWrapper _jwtbuilder;
        public const string IssuerConfigName = "JWT:Issuer";
        public const string AudienceConfigName = "JWT:Audience";

        public JwtTokenUtil(IConfiguration configuration, JwtBuilderWrapper jwtBuilder)
        {
            _issuer = configuration[IssuerConfigName];
            _audience = configuration[AudienceConfigName];
            _jwtbuilder = jwtBuilder;
        }

        public string Make(string userId)
        {
            return _jwtbuilder.Encode(new JwtToken()
            {
                //_issuer, _audience, userId
                Issuer = _issuer,
                Audience = _audience,
                Id = userId,
                CreateTime = DateTime.Now
            });
        }

        public bool TryDecode(string token, out JwtToken? jwtToken)
        {
            var hasDecoded = _jwtbuilder.TryDecode(token, out jwtToken);
            return hasDecoded;
        }

        public JwtToken Decode(string token)
        {
            var tokenObj = _jwtbuilder.Decode<JwtToken>(token);
            return tokenObj;
        }

    }
}
