﻿namespace IndexOut.Api.Utils
{
    public class RandomUtil
    {
        public readonly Random _random;

        public RandomUtil()
        {
            _random = new Random();
        }

        public string RandomName()
        {
            var words = "qwertyuiopastyuiopasdfghjklzdfghjklzxcvbnm_-";

            var name = "";
            var nameLength = _random.Next(6, 10);
            for (var i = 0; i < nameLength; i++)
            {
                name += words[_random.Next(0, words.Length - 1)];
            }
            return name;
        }
    }
}
