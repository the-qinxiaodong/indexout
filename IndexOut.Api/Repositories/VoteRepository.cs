﻿using IndexOut.Api.Entities;
using Microsoft.EntityFrameworkCore;

namespace IndexOut.Api.Repositories
{
    public class VoteRepository : RepositoryBase<Vote>
    {
        private readonly DbSet<Vote> _voteSet;

        public VoteRepository(DbContext dbContext) : base(dbContext)
        {
            _voteSet = dbContext.Set<Vote>();
        }

        //public new async void SaveAsync(Vote vote)
        //{
        //    await _voteSet.AddAsync(vote);
        //}

        public new async Task<Vote?> FindByIDAsync(int voteId)
        {
            return await _voteSet.Where(vote => vote.Id == voteId)
                .Include(vote => vote.Author)
                .Include(vote => vote.Topic)
                .Include(vote => vote.Options)
                .Include(vote => vote.Tags)
                .FirstOrDefaultAsync();
        }
    }
}