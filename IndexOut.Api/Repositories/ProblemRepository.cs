﻿using IndexOut.Api.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace IndexOut.Api.Repositories
{
    public class ProblemRepository : RepositoryBase<Problem>
    {
        private readonly DbSet<Problem> _problemSet;

        public ProblemRepository(DbContext dbContext) : base(dbContext)
        {
            _problemSet = _dbContext.Set<Problem>();
        }

        public async Task<IEnumerable<Problem>> GetPaginationAsync(int pageNumber, int pageSize, Expression<Func<Problem, bool>> problemSelector)
        {
            return await _problemSet
                .Where(problemSelector)
                .Include(problem => problem.Topic)
                .Include(problem => problem.Tags)
                .Include(problem => problem.Author)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .ToArrayAsync();
        }

        public async Task<IEnumerable<Problem>> GetPaginationAsync(int pageNumber, int pageSize)
        {
            return await GetPaginationAsync(pageNumber, pageSize, problem => true);
        }

        public async Task<Problem?> GetProblemAsync(int problemId)
        {
            return await _problemSet.Where(problem => problem.Id == problemId)
                .Include(problem => problem.Topic)
                .Include(problem => problem.Tags)
                .Include(problem => problem.Author)
                .FirstOrDefaultAsync();
        }

        public void DeleteById(int problemId)
        {
            var fakeProblem = new Problem { Id = problemId };
            _dbContext.Attach(fakeProblem);
            _problemSet.Remove(fakeProblem);
        }

        public async Task<bool> TestExistsAsync(int problemId)
        {
            var count = await _problemSet.Where(problem => problem.Id == problemId).CountAsync();
            return count > 0;
        }
    }
}
