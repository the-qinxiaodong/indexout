﻿using AutoMapper;
using IndexOut.Api.Entities;
using IndexOut.Api.Models;
using Microsoft.EntityFrameworkCore;

namespace IndexOut.Api.Repositories
{
    public class ArticleRepository : RepositoryBase<Article>
    {
        public readonly MemberRespository _memberRepository;
        private readonly IMapper _mapper;
        private readonly TagRepository _tagRepository;

        public ArticleRepository(MemberRespository memberRepository, TagRepository tagRepository, IMapper mapper, DbContext dbContext) : base(dbContext)
        {
            _memberRepository = memberRepository;
            _mapper = mapper;
            _tagRepository = tagRepository;
        }

        public async Task<(bool, ArticleModel?)> TryCreateArticleAsync(CreationArticleModel creationArticleModel, int memberId)
        {
            var article = _mapper.Map<Article>(creationArticleModel);
            var author = new Member() { Id = memberId };
            var topic = new Topic()
            {
                Id = creationArticleModel.Topic! ?? throw new NotSupportedException($"抛出此异常说明程序逻辑错误，请检查{nameof(CreationArticleModel)}")
            };

            _dbContext.Attach(author);
            _dbContext.Attach(topic);

            if (await _memberRepository.VerfyMemberExistsByIdAsync(memberId))
            {
                var tags = new List<Tag>();
                foreach (var tagValue in creationArticleModel.Tags)
                {
                    var tag = await _tagRepository.GetAndCreateTagIfNotExistsAsync(tagValue);
                    tags.Add(tag);
                }

                article.Author = author;
                article.Tags = tags;
                article.Topic = topic;

                await SaveAsync(article);
                return (true, _mapper.Map<ArticleModel>(article));
            }

            return (false, null);
        }

        public async Task<Article?> FindByAuthorAndIdAsync(int memberId, int articleId)
        {
            return await _dbContext.Set<Article>()
                .Where(article => articleId == article.Id && article.Author!.Id == memberId)
                .Include(article => article.Author)
                .Include(article => article.Tags)
                .Include(article => article.Topic)
                .FirstOrDefaultAsync();
        }

        public async Task<IList<Article>> GetPaginationOfAuthorAsync(int memberId, int pageNumber, int pageSize)
        {
            return await _dbContext.Set<Article>()
                .OrderBy(article => article.Id)
                .Where(article => article.Author!.Id == memberId)
                .Skip(pageSize * (pageNumber - 1))
                .Take(pageSize)
                .Include(article => article.Author)
                .Include(article => article.Topic)
                .Include(article => article.Tags)
                .ToListAsync();
        }

        public async Task<bool> TestArticleExistsAsync(int articleId)
        {
            var articleCount = await _dbContext.Set<Article>()
                .Where(article => article.Id == articleId)
                .CountAsync();
            return articleCount > 0;
        }

        public async Task<Article?> FindByIDAndLoadRelationshipAsync(int articleId)
        {
            return await _dbContext.Set<Article>()
                .Where(article => article.Id == articleId)
                .Include(article => article.Author)
                .Include(article => article.Tags)
                .Include(article => article.Topic)
                .FirstOrDefaultAsync();
        }
    }
}
