﻿using AutoMapper;
using IndexOut.Api.Entities;
using IndexOut.Api.Models;
using IndexOut.Api.Services;
using IndexOut.Api.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Primitives;
using System.Net.Mail;
using IndexOut.Api.Extensions;

namespace IndexOut.Api.Repositories
{
    public class MemberRespository : RepositoryBase<Member>
    {
        private readonly IMapper _mapper;
        private readonly EmailService _emailService;
        private readonly BCryptService _bCryptService;
        private readonly JwtBuilderWrapper _jwtBuilder;

        public MemberRespository(DbContext dbContext, IMapper mapper, EmailService emailService, BCryptService bCryptService, JwtBuilderWrapper jwtBuilder) : base(dbContext)
        {
            _mapper = mapper;
            _emailService = emailService;
            _bCryptService = bCryptService;
            _jwtBuilder = jwtBuilder;
        }

        public async Task<(bool, MemberProfileModel?)> TryGetProfileAsync(int memberId)
        {
            var member = await _dbContext.Set<Member>().Where(member => member.Id == memberId).FirstOrDefaultAsync();
            if (member == null)
                return (false, null);

            return (true, _mapper.Map<MemberProfileModel>(member));
        }

        public async Task<bool> TryChangePasswordAsync(string newPassword, string oldPassword, int uid)
        {
            var member = await _dbContext.Set<Member>().Where(member => member.Id == uid).FirstOrDefaultAsync();

            if (member != null && _bCryptService.Verify(oldPassword, member.Password))
            {
                member.Password = _bCryptService.Hash(newPassword);
                return true;
            }

            return false;
        }

        public async Task<bool> TryUpdateProfileAsync(UpdateProfileModel updateProfileModel)
        {
            var memberExists = await VerfyMemberExistsByIdAsync(updateProfileModel.Id);
            if (memberExists)
            {
                var member = _mapper.Map<Member>(updateProfileModel);
                var fakeMember = new Member()
                {
                    Id = member.Id,
                };
                _dbContext.Attach(fakeMember);
                member!.CopyTo(fakeMember, null, nameof(Member.CreatedDate));
                return true;
            }

            return false;
        }

        public async Task<bool> VerfyMemberExistsByIdAsync(int id)
        {
            var memberCount = await _dbContext.Set<Member>().Where(member => member.Id == id).CountAsync();
            return memberCount > 0;
        }

        public async Task<(bool, int)> TryVerifyLoginTokenAsync(StringValues token)
        {
            var hasDecoded = _jwtBuilder.TryDecode<JwtToken>(token, out var jwtToken);

            if (hasDecoded && !jwtToken!.IsExpired)
            {
                var idString = jwtToken!.Id;
                var hasParsed = int.TryParse(idString, out var id);
                if (!hasParsed)
                    return (false, 0);


                var memberCount = await (from member in _dbContext.Set<Member>()
                                         where member.Id == id
                                         select member).CountAsync();
                return (memberCount > 0, id);
            }

            return (false, 0);
        }

        public async Task<bool> HasRegisteredAsync(MailAddress mailAddress)
        {
            var mailString = mailAddress.ToString();
            var id = await (
                from member in _dbContext.Set<Member>()
                where member.Email == mailString
                select member.Id).FirstOrDefaultAsync();
            return id != 0;
        }

        public async Task<bool> RegisterAsync(RegisterModel registerModel)
        {
            var authCodeValue = registerModel.AuthCode;
            var registeringEmail = registerModel.Email;
            var authCode = await FindByValueAndEmailAsync(authCodeValue, registeringEmail);

            if (authCode != null && !authCode.IsExpires)
            {
                var member = _mapper.Map<Member>(registerModel);
                member.Password = _bCryptService.Hash(member.Password);
                await _dbContext.AddAsync(member);
                return true;
            }

            return false;
        }

        public async Task<(bool, Member?)> LoginAsync(LoginModel loginModel)
        {
            var email = loginModel.Email;
            var username = loginModel.Name;
            var password = loginModel.Password;

            var dbset = _dbContext.Set<Member>();
            var query = default(IQueryable<Member?>);

            if (email != null)
            {
                query = dbset.Where(member => member.Email == email);
            }
            else
            {
                query = dbset.Where(member => member.Name == username);
            }

            var member = await query.FirstOrDefaultAsync();
            if (member == null)
                return (false, null);

            return (_bCryptService.Verify(password, member.Password), member);
        }

        private async Task<RegistrationCode?> FindByValueAndEmailAsync(int authCodeValue, string registeringEmail)
        {
            return await (from authCode in _dbContext.Set<RegistrationCode>()
                          where authCode.OwnerEmail == registeringEmail && authCode.Value == authCodeValue
                          orderby authCode.CreatedDate descending
                          select authCode).SingleOrDefaultAsync();
        }

        public async Task<bool> TrySendResetPasswordEmailIfValidModelAsync(ApplyForResetPasswordModel resetPasswordModel)
        {
            var email = resetPasswordModel.Email;
            if (email == null)
            {
                var requestNameReuslt = await (from member in _dbContext.Set<Member>()
                                               where member.Name == resetPasswordModel.Name
                                               select new { member.Email }).FirstOrDefaultAsync();
                // 无效的用户名
                if (requestNameReuslt == null)
                    return false;

                email = requestNameReuslt.Email;
            }

            var memberByEmail = await _dbContext.Set<Member>().Where(member => member.Email == email).FirstOrDefaultAsync();

            // 无效的邮箱
            if (memberByEmail == null)
            {
                return false;
            }

            var resetPasswordToken = MakeResetPasswordToken(memberByEmail);
            await _emailService.SendEmailAsync("您正在重置IndexOut密码", $"密码重置令牌为{resetPasswordToken}", new MailAddress(email));
            return true;
        }

        public async Task<bool> TryResetPassword(ResetPasswordModel resetPasswordModel)
        {
            var hasDecoded = _jwtBuilder.TryDecode<ResetPasswordToken>(resetPasswordModel.ResetPasswordToken, out var resetPasswordToken);

            if (hasDecoded && !resetPasswordToken!.IsExpires)
            {
                var uid = resetPasswordToken!.MemberId;
                var password = resetPasswordModel.Password;
                var member = await FindByIDAsync(uid);
                if (member != null)
                {
                    member.Password = _bCryptService.Hash(password);
                    return true;
                }

            }

            return false;
        }

        private string MakeResetPasswordToken(Member member)
        {
            var resetPasswordToken = new ResetPasswordToken()
            {
                MemberId = member.Id,
            };

            return _jwtBuilder.Encode(resetPasswordToken);
        }

    }
}
