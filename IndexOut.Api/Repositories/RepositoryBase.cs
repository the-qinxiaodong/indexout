﻿using IndexOut.Api.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace IndexOut.Api.Repositories
{
    public class RepositoryBase<T> : IRepository<T> where T : EntityBase
    {
        protected readonly DbContext _dbContext;

        public RepositoryBase(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task DeleteAsync(T entity)
        {
            var context = new SqlContext();
            _dbContext.Remove(entity);
            return Task.CompletedTask;
        }

        public async Task<T?> FindByIDAsync(int id)
        {
            return await _dbContext.FindAsync<T>(id);
        }

        public async Task SaveAsync(T entity)
        {
            await _dbContext.AddAsync(entity);
            _dbContext.SaveChanges();
        }

        public async Task UpdateAsync(EntityEntry<T> entity)
        {
            await _dbContext.SaveChangesAsync();
        }
    }
}
