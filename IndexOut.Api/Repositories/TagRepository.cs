﻿using IndexOut.Api.Entities;
using Microsoft.EntityFrameworkCore;

namespace IndexOut.Api.Repositories
{
    public class TagRepository : RepositoryBase<Tag>
    {
        public TagRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public async Task<Tag> CreateTagByValueAsync(string tagValue)
        {
            var tag = new Tag()
            {
                Value = tagValue
            };
            await SaveAsync(tag);
            return tag;
        }

        public async Task<Tag> GetAndCreateTagIfNotExistsAsync(string tagValue)
        {
            var tag = await _dbContext.Set<Tag>().Where(tag => tag.Value == tagValue).FirstOrDefaultAsync();

            if (tag == null)
            {
                return await CreateTagByValueAsync(tagValue);
            }

            return tag;
        }

        public async Task<IEnumerable<int>> GetNotExistsFromAsync(IEnumerable<int> tagIdCollection)
        {
            var tagIdList = tagIdCollection.ToList();
            var existsTagInList = await _dbContext.Set<Tag>()
                .Where(tag => tagIdList.Contains(tag.Id))
                .Select(tag => tag.Id)
                .ToArrayAsync();

            return tagIdList.Where(id => !existsTagInList.Contains(id));
        }
    }
}
