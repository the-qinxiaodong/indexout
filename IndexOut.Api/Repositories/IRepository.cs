﻿using IndexOut.Api.Entities;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace IndexOut.Api.Repositories
{
    public interface IRepository<TEntity> where TEntity : EntityBase
    {
        public Task<TEntity?> FindByIDAsync(int id);
        public Task DeleteAsync(TEntity entity);
        public Task SaveAsync(TEntity entity);
        public Task UpdateAsync(EntityEntry<TEntity> entity);
    }
}
