﻿using IndexOut.Api.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace IndexOut.Api.Repositories
{
    public class CommentRepository : RepositoryBase<Comment>
    {
        private readonly DbSet<Comment> _commentSet;

        public CommentRepository(DbContext dbContext) : base(dbContext)
        {
            _commentSet = dbContext.Set<Comment>();
        }

        public async Task<IEnumerable<Comment>> GetPaginationAsync(int pageNumber, int pageSize, Expression<Func<Comment, bool>> commentSelector)
        {
            return await _commentSet
                .Include(comment => comment.Author)
                .OrderBy(comment => comment.Id)
                .Where(commentSelector)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .ToArrayAsync();
        }
    }
}
