﻿using IndexOut.Api.Entities;
using Microsoft.EntityFrameworkCore;

namespace IndexOut.Api.Repositories
{
    public class TopicRepository : RepositoryBase<Topic>
    {
        public TopicRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public async Task<bool> TestExistsAsync(int id)
        {
            var count = await _dbContext.Set<Topic>().Where(topic => topic.Id == id).CountAsync();
            return count > 0;
        }

        public async Task<Topic?> FindTopicByValueAsync(string value)
        {
            return await _dbContext.Set<Topic>().Where(topic => topic.Value == value).FirstOrDefaultAsync();
        }
    }
}
