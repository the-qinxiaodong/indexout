﻿using IndexOut.Api.Entities;
using Microsoft.EntityFrameworkCore;

namespace IndexOut.Api.Repositories
{
    public class VoteOptionRepository : RepositoryBase<VoteOption>
    {
        public VoteOptionRepository(DbContext dbContext) : base(dbContext)
        {
        }
    }
}
