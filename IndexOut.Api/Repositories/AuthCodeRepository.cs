﻿using IndexOut.Api.Entities;
using Microsoft.EntityFrameworkCore;
using System.Net.Mail;

namespace IndexOut.Api.Repositories
{
    public class RegistrationCodeRepository : RepositoryBase<RegistrationCode>
    {
        private readonly MemberRespository _memberRespository;

        public RegistrationCodeRepository(DbContext dbContext, MemberRespository memberRespository) : base(dbContext)
        {
            _memberRespository = memberRespository;
        }

        public async Task<(bool, RegistrationCode?)> TryApplyForRegistrationCodeAsync(MailAddress mailAddress)
        {
            if (await _memberRespository.HasRegisteredAsync(mailAddress))
            {
                return (false, null);
            }

            var randomInt = new Random().Next(10000, 99999);
            var registrationCode = new RegistrationCode()
            {
                OwnerEmail = mailAddress.ToString(),
                Value = randomInt
            };
            await _dbContext.AddAsync(registrationCode);
            return (true, registrationCode);
        }
    }
}
