﻿namespace IndexOut.Api.Entities
{
    public class Article : Content
    {
        public string? Title { get; set; }
        public ArticleState ArticleState { get; set; }
    }

    public enum ArticleState
    {
        Writing, Published
    }
}
