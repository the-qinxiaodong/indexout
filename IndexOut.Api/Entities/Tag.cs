﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace IndexOut.Api.Entities
{
    [Index("Value")]
    public class Tag : EntityBase
    {
        [Required]
        public string Value { get; set; } = string.Empty;
    }
}
