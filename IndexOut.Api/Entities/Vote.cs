﻿namespace IndexOut.Api.Entities
{
    public class Vote : Content
    {
        public IList<VoteOption> Options { get; set; } = new List<VoteOption>();

        public VoteState State { get; set; }

        public DateTime? EndTime { get; set; }
    }

    public enum VoteState
    {
        Writing, Published, End
    }
}
