﻿using System.ComponentModel.DataAnnotations;

namespace IndexOut.Api.Entities
{
    public abstract class EntityBase
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; } = DateTime.Now;
    }
}
