﻿namespace IndexOut.Api.Entities
{
    public class Comment : EntityBase
    {
        public Member? Author { get; set; }
        public Content? To { get; set; }
        public string Value { get; set; } = string.Empty;
    }
}