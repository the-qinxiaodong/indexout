﻿using System.ComponentModel.DataAnnotations;

namespace IndexOut.Api.Entities
{
    public class Problem : Content
    {
        [Required]
        [StringLength(100, MinimumLength = 2)]
        public string? Title { get; set; }

        [Required]
        public int Reward { get; set; }

        [Required]
        public ProblemState State { get; set; }

        public IList<Answer> Answers { get; set; } = new List<Answer>();
    }

    public enum ProblemState
    {
        Writing, Published, Answered
    }
}
