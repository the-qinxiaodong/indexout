﻿using System.ComponentModel.DataAnnotations.Schema;

namespace IndexOut.Api.Entities
{
    public class VoteOption : EntityBase
    {
        public string Value { get; set; } = string.Empty;

        public Vote? Vote { get; set; }

        public int VoteNumber { get; set; }
    }
}