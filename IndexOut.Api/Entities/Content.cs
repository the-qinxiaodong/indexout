﻿using System.ComponentModel.DataAnnotations;

namespace IndexOut.Api.Entities
{
    public class Content : EntityBase
    {
        [Required]
        public string Value { get; set; } = string.Empty;

        [Required]
        public Member? Author { get; set; }

        public IList<Comment> Comments { get; set; } = new List<Comment>();
        public IList<Tag> Tags { get; set; } = new List<Tag>();
        public Topic? Topic { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime PublishTime { get; set; }
    }
}
