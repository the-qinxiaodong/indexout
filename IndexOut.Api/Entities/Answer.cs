﻿namespace IndexOut.Api.Entities
{
    public class Answer : Content
    {
        public AnswerState State { get; set; }

        public Problem? ForProblem { get; set; }
    }

    public enum AnswerState
    {
        Writing, Published, Adopted
    }
}