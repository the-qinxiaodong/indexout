﻿using IndexOut.Api.Utils;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace IndexOut.Api.Entities
{
    [Index(nameof(Name), IsUnique = true)]
    [Index(nameof(Email), IsUnique = true)]
    public class Member : EntityBase
    {
        public static readonly RandomUtil _randomUtil = new RandomUtil();

        [Required]
        public string Name { get; set; } = string.Empty;

        [Required]
        public string Email { get; set; } = string.Empty ;

        [Required]
        public string Password { get; set; } = string.Empty;

        [Required]
        public Gender Gender { get; set; }

        public string? Introduction { get; set; }

        [DataType(DataType.Date)]
        public DateTime? Birthday { get; set; }

        public string NickName { get; set; } = _randomUtil.RandomName();

        public string? PersonalSignature { get; set; }

        public int Credit { get; set; }

        public int Money { get; set; }
    }

    public enum Gender
    {
        NotSet, Male, Female
    }
}
