﻿using System.ComponentModel.DataAnnotations;

namespace IndexOut.Api.Entities
{
    public class InvitionCode : EntityBase
    {
        [Required]
        public string? Value { get; set; }

        [Required]
        public Member? Inviter { get; set; }

        [Required]
        public Member? Invitee { get; set; }
    }
}
