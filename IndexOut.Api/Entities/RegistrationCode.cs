﻿using Microsoft.EntityFrameworkCore;

namespace IndexOut.Api.Entities
{
    [Index(nameof(OwnerEmail), nameof(Value))]
    public class RegistrationCode : EntityBase
    {
        private static readonly TimeSpan _fiveMinutes = new TimeSpan(0, 5, 0);

        public int Value { get; set; }

        public string OwnerEmail { get; set; } = string.Empty;

        public bool IsExpires => DateTime.Now - CreatedDate
            > _fiveMinutes;
    }
}
