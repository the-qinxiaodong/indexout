﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace IndexOut.Api.Entities
{
    [Index(nameof(Value), IsUnique = true)]
    public class Topic : EntityBase
    {
        [Required]
        public string Value { get; set; } = string.Empty;
    }
}
