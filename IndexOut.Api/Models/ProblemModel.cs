﻿using IndexOut.Api.Entities;

namespace IndexOut.Api.Models
{
    public class ProblemModel
    {
        public int Id { get; set; }
        public string Title { get; set; } = string.Empty;
        public string Value { get; set; } = string.Empty;

        public MemberModel? Author { get; set; }

        public IList<string> Tags { get; set; } = new List<string>();
        public string? Topic { get; set; }

        public DateTime PublishTime { get; set; }

        public int Reward { get; set; }

        public ProblemState State { get; set; }

    }
}