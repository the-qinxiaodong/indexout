﻿namespace IndexOut.Api.Models
{
    public class ResetPasswordToken
    {
        private readonly TimeSpan _fiveMinutes = new TimeSpan(0, 5, 0);

        public TokenType TokenType => TokenType.ResetPasswordToken;
        public int MemberId { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public bool IsExpires => DateTime.Now - CreatedDate > _fiveMinutes;
    }
}
