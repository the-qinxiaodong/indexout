﻿using IndexOut.Api.Entities;

namespace IndexOut.Api.Models
{
    public class VoteModel
    {
        public int Id { get; set; }
        public string Value { get; set; } = string.Empty;

        public MemberModel? Author { get; set; }

        public IList<string> Tags { get; set; } = new List<string>();
        public string? Topic { get; set; }

        public DateTime PublishTime { get; set; }

        public IList<string> Options { get; set; } = new List<string>();

        public VoteState State { get; set; }

        public DateTime? EndTime { get; set; }
    }
}