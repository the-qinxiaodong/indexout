﻿using IndexOut.Api.ValidationAttributes;

namespace IndexOut.Api.Models
{
    [EmailOrNameMustNotNull]
    public class ApplyForResetPasswordModel
    {
        public string? Email { get; set; }
        public string? Name { get; set; }
    }
}
