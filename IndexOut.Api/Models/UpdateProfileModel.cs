﻿using IndexOut.Api.Entities;
using System.ComponentModel.DataAnnotations;

namespace IndexOut.Api.Models
{
    public class UpdateProfileModel
    {
        [Required]
        public int Id { get; set; }

        public string Name { get; set; } = string.Empty;

        public string Email { get; set; } = string.Empty;

        public Gender Gender { get; set; }

        public string? Introduction { get; set; }

        public DateTime? Birthday { get; set; }

        public string? NickName { get; set; }

        public string? PersonalSignature { get; set; }
    }
}