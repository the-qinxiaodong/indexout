﻿using IndexOut.Api.Entities;
using System.ComponentModel.DataAnnotations;

namespace IndexOut.Api.Models
{
    public class RegisterModel
    {
        [Required]
        [StringLength(50, MinimumLength =2)]
        public string Name { get; set; } = string.Empty;

        [DataType(DataType.EmailAddress)]
        [Required]
        public string Email { get; set; }= string.Empty;

        [DataType(DataType.Password)]
        [Required]
        public string Password { get; set; } = string.Empty;

        [Required]
        public int AuthCode { get; set; }

        public Gender Gender { get; set; }

        public string? Introduction { get; set; }

        public DateTime? Birthday { get; set; }

        public string? NickName { get; set; }

        public string? PersonalSignature { get; set; }

    }
}
