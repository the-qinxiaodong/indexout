﻿using IndexOut.Api.Entities;

namespace IndexOut.Api.Models
{
    public class ArticleModel
    {
        public int Id { get; set; }

        public int Author { get; set; }

        public string? Title { get; set; }

        public string? Value { get; set; }

        public IList<string>? Tags { get; set; }

        public int? Topic { get; set; }

        public ArticleState ArticleState { get; set; }

        public DateTime PublishTime { get; set; }
    }
}
