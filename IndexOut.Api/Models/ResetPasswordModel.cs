﻿using System.ComponentModel.DataAnnotations;

namespace IndexOut.Api.Models
{
    public class ResetPasswordModel
    {
        [Required]
        public string ResetPasswordToken { get; set; } = string.Empty;

        [Required]
        [DataType(DataType.Password)]
        [StringLength(50, MinimumLength = 8)]
        public string Password { get; set; } = string.Empty;
    }
}
