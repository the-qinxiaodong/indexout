﻿namespace IndexOut.Api.Models
{
    public class CommentModel
    {
        public int Id { get; set; }
        public int Author { get; set; }
        public string Value = string.Empty;
    }
}
