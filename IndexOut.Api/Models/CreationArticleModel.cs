﻿using IndexOut.Api.Attributes;
using System.ComponentModel.DataAnnotations;

namespace IndexOut.Api.Models
{
    public class CreationArticleModel
    {
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string? Title { get; set; }

        [Required]
        public string Value { get; set; } = string.Empty;

        [NoMap]
        public IList<string> Tags { get; set; } = new List<string>();

        [NoMap]
        public int? Topic { get; set; }

    }
}
