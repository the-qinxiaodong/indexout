﻿namespace IndexOut.Api.Models
{
    public class JwtToken
    {
        private static readonly TimeSpan _ValidTime = new TimeSpan(0, 30, 0);

        public string Issuer { get; set; } = string.Empty;

        public string Audience { get; set; } = string.Empty;

        public string Id { get; set; } = string.Empty;

        public DateTime CreateTime { get; set; }

        public bool IsExpired => DateTime.Now - CreateTime > _ValidTime;

        //public JwtToken(string issuer, string audience, string id)
        //{
        //    this.Issuer = issuer;
        //    this.Audience = audience;
        //    this.Id = id;
        //    this.CreateTime = DateTime.Now;
        //}
    }
}
