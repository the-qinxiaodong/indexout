﻿using IndexOut.Api.Entities;

namespace IndexOut.Api.Models
{
    public class MemberModel
    {
        public int Id { get; set; }

        public string Name { get; set; } = string.Empty;

        public Gender Gender { get; set; }

        public string? Introduction { get; set; }

        public DateTime? Birthday { get; set; }

        public string? NickName { get; set; }

        public string? PersonalSignature { get; set; }

        public int Credit { get; set; }

        public int Money { get; set; }
    }
}
