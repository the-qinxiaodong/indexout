﻿using IndexOut.Api.ValidationAttributes;
using System.ComponentModel.DataAnnotations;

namespace IndexOut.Api.Models
{
    [EmailOrNameRequiredOnLoginModel]
    public class LoginModel
    {
        public string? Email { get; set; }

        public string? Name { get; set; }

        [Required]
        public string Password { get; set; } = string.Empty;
    }
}
