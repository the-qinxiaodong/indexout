﻿using IndexOut.Api.Entities;
using System.ComponentModel.DataAnnotations;

namespace IndexOut.Api.Models
{
    public class CreationVoteModel
    {
        [Required]
        public string Value { get; set; } = string.Empty;

        [Required]
        public IList<string> Tags { get; set; } = new List<string>();

        [Required]
        public int Topic { get; set; }

        [Required]
        public IList<string> Options { get; set; } = new List<string>();
    }
}
