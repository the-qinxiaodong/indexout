﻿using IndexOut.Api.Entities;

namespace IndexOut.Api.Models
{
    public class UpdateProblemModel
    {
        public string? Title { get; set; }
        public string? Value { get; set; }
        public IList<string> Tags { get; set; } = new List<string>();
        public void ApplyTo(Problem problem)
        {
            if (Title != null)
                problem.Title = Title;
            if (Value != null)
                problem.Value = Value;
            if (Tags != null && Tags.Count > 0)
                problem.Tags = Tags
                    .Select(tagValue => new Tag() { Value = tagValue, CreatedDate = DateTime.Now })
                    .ToList();
        }
    }
}
