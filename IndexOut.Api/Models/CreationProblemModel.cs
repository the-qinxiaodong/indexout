﻿using System.ComponentModel.DataAnnotations;

namespace IndexOut.Api.Models
{
    public class CreationProblemModel
    {
        [Required]
        [StringLength(100, MinimumLength = 2)]
        public string Title { get; set; } = string.Empty;

        [Required]
        public int Reward { get; set; }

        [Required]
        public string Value { get; set; } = string.Empty;

        [Required]
        public IList<string> Tags { get; set; } = new List<string>();

        [Required]
        public string? Topic { get; set; }
    }
}