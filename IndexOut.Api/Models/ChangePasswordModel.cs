﻿using System.ComponentModel.DataAnnotations;

namespace IndexOut.Api.Models
{
    public class ChangePasswordModel
    {
        [Required]
        public string NewPassword { get; set; } = string.Empty;

        [Required]
        public string OldPassword { get; set; } = string.Empty;
    }
}
