﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IndexOut.Api.Migrations
{
    public partial class CommentAddValueField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Value",
                table: "Comment",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Value",
                table: "Comment");
        }
    }
}
