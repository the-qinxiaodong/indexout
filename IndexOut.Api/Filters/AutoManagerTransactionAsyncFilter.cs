﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;

namespace IndexOut.Api.Filters
{
    public class AutoManagerTransactionAsyncFilter : IAsyncActionFilter
    {
        private readonly DbContext _dbContext;

        public AutoManagerTransactionAsyncFilter(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var transaction = await _dbContext.Database.BeginTransactionAsync();
            using (_dbContext)
            {
                using (transaction)
                {
                    var actionExecutedContext = await next.Invoke();
                    if (actionExecutedContext.Exception != null)
                    {
                        transaction.Rollback();
                    }
                    else
                    {
                        transaction.Commit();
                        _dbContext.SaveChanges();
                    }
                }
            }
        }
    }
}
