﻿using IndexOut.Api.Repositories;
using IndexOut.Api.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace IndexOut.Api.Filters
{
    public class LoginRequiredAsyncFilterAttribute : ActionFilterAttribute, IAsyncAuthorizationFilter
    {
        private readonly MemberRespository _memberRepository;
        private readonly JwtTokenUtil _jwtTokenUtils;

        public LoginRequiredAsyncFilterAttribute(MemberRespository memberRepository, JwtTokenUtil jwtTokenUtils)
        {
            _memberRepository = memberRepository;
            _jwtTokenUtils = jwtTokenUtils;
        }

        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            var request = context.HttpContext.Request;
            var hasToken = request.Headers.TryGetValue("token", out var token);

            if (!hasToken)
                context.Result = new JsonResult("请先登录") { StatusCode = StatusCodes.Status401Unauthorized };

            var (valid, memberId) = await _memberRepository.TryVerifyLoginTokenAsync(token);
            if (!valid)
                context.Result = new BadRequestResult();

            var memberIdOnPath = context.RouteData.Values.GetValueOrDefault("memberId") as int?;
            if (memberIdOnPath != null && memberIdOnPath != memberId)
                context.Result = new BadRequestResult();

            context.HttpContext.Items["memberId"] = memberId;
        }
    }
}
