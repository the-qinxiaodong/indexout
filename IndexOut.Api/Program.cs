using IndexOut.Api;
using IndexOut.Api.Filters;
using IndexOut.Api.Profiles;
using IndexOut.Api.Repositories;
using IndexOut.Api.Services;
using IndexOut.Api.Utils;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddSingleton<JwtBuilderWrapper>();
builder.Services.AddSingleton<JwtTokenUtil>();
builder.Services.AddSingleton<EmailProfile>();
builder.Services.AddSingleton<BCryptService>();

builder.Services.AddScoped<DbContext, SqlContext>();
builder.Services.AddScoped<MemberRespository>();
builder.Services.AddScoped<RegistrationCodeRepository>();
builder.Services.AddScoped<EmailService>();
builder.Services.AddScoped<LoginRequiredAsyncFilterAttribute>();
builder.Services.AddScoped<ArticleRepository>();
builder.Services.AddScoped<TagRepository>();
builder.Services.AddScoped<ProblemRepository>();
builder.Services.AddScoped<TopicRepository>();
builder.Services.AddScoped<VoteRepository>();
builder.Services.AddScoped<VoteOptionRepository>();
builder.Services.AddScoped<CommentRepository>();


builder.Services.AddTransient<SMTPClientWrapper>();

builder.Services.AddAutoMapper(typeof(AutoMapperProfile));

// Add services to the container.
builder.Services
    .AddControllers(configure => configure.Filters.Add<AutoManagerTransactionAsyncFilter>())
    .AddNewtonsoftJson();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
}

app.UseRouting();

//app.UseAuthorization();

app.MapControllers();

app.Run();
