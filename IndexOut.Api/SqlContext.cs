﻿using IndexOut.Api.Entities;
using Microsoft.EntityFrameworkCore;

namespace IndexOut.Api
{
    public class SqlContext : DbContext
    {
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<EntityBase>()
            //    .Property(e => e.CreatedDate).HasDefaultValue(DateTime.Now);
            modelBuilder.Entity<RegistrationCode>().ToTable("RegistrationCode");
            modelBuilder.Entity<Article>().ToTable("Article");
            modelBuilder.Entity<Comment>().ToTable("Comment");
            modelBuilder.Entity<Answer>().ToTable("Answer");
            modelBuilder.Entity<InvitionCode>().ToTable("InvitionCode").HasOne(code => code.Inviter).WithMany().OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<Member>().ToTable("Member");
            modelBuilder.Entity<Problem>().ToTable("Problem");
            modelBuilder.Entity<Tag>().ToTable("Tag");
            modelBuilder.Entity<Topic>().ToTable("Topic");
            modelBuilder.Entity<Vote>().ToTable("Vote");
            modelBuilder.Entity<VoteOption>().ToTable("VoteOption");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=IndexOut;Integrated Security=True;");

#if DEBUG
            optionsBuilder.EnableSensitiveDataLogging().LogTo(Console.WriteLine);
#endif
        }
    }
}
