﻿using IndexOut.Api.Utils;
using System.Net.Mail;

namespace IndexOut.Api.Services
{
    public class EmailService
    {
        private readonly SMTPClientWrapper _smtpClient;

        public EmailService(SMTPClientWrapper smtpClient)
        {
            _smtpClient = smtpClient;
        }

        public async Task SendRegistrationCodeEmailAsync(int registrationCode, MailAddress mailAddress)
        {
            await _smtpClient.SendAsync("您正在注册IndexOut", $"您的验证码是{registrationCode}", mailAddress);
        }

        public async Task SendEmailAsync(string title, string content, MailAddress to)
        {
            await _smtpClient.SendAsync(title, content, to);
        }
    }
}
